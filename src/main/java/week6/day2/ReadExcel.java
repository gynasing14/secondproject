package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	//@Test
	public static Object[][] readExcel() throws IOException {

		//Open the Excel
		XSSFWorkbook wb = new XSSFWorkbook("./data/sampleFile.xlsx");

		//GoTo Sheet
		XSSFSheet sheet = wb.getSheetAt(0);
		
		//rowcount
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		
		//cell count
		int cellCount= sheet.getRow(0).getLastCellNum();
		System.out.println(cellCount);
		
		Object[][] data = new Object[rowCount][cellCount];

		for (int j = 1; j < rowCount; j++) {
			//GoTo Specific Row
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < cellCount; i++) {
				//GoTo Specific Cell
				XSSFCell cell = row.getCell(i);
				//read the content(String)
				String value;
				try {
					value = cell.getStringCellValue();
					System.out.println(value);
					data[j-1][i]= value;
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					System.out.println("");
				}
				
			} 
		}
		//close Excel
		wb.close();
		return data;
	}
}
