package dailychallenges;

import java.util.Scanner;

public class FizzBuzzTwoInputs {

	public static void main(String[] args) {
		Scanner scan =new Scanner(System.in);
		System.out.println("Enter value 1 : ");
		int num1 = scan.nextInt();
		System.out.println("Enter value 2 : ");
		int num2 = scan.nextInt();
		for(int i = num1; i<=num2; i++) {
			if((i%3 == 0) && (i%5 == 0)) {
				System.out.println("FIZZBUZZ");
			}else if(i%3 == 0) {
				System.out.println("FIZZ");
			}else if(i%5 == 0) {
				System.out.println("BUZZ");
			}else {
				System.out.println(i);
			}
		}
	}

}
