package week2.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MaxPriceMobile {

	public static void main(String[] args) {
		Map<String,Integer> map = new HashMap<>();
		map.put("Nokia", 35000);
		map.put("Apple", 80000);
		map.put("Samsung",50000);
		for (Entry<String, Integer> mob : map.entrySet()) {
			if(mob.getValue() > 50000) {
				System.out.println(mob.getKey() +"-->"+mob.getValue());
			}
		}
		}

}
