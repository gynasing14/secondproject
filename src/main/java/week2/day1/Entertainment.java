package week2.day1;

public interface Entertainment {
	public void powerBackup();
	public void comedyShow();
	public void news();
}
