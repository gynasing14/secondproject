package dailychallenges;

import java.util.Scanner;

public class ArithmeticSwitchOperations {

	public static void main(String[] args) {
		int result = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter 1st value : ");
		int num1 = scan.nextInt();
		System.out.println("Enter 2nd value : ");
		int num2 = scan.nextInt();
		System.out.println("Enter ur choice to perform respective arithmetic operation : ");
		String choice = scan.next();
		switch(choice) {
		case "add": 
			result = num1+num2;
			break;
		case "sub": 
			result = num1-num2;
			break;
		case "mul": 
			result = num1*num2;
			break;
		case "div": 
			result = num1/num2;
			break;
		default:
			System.out.println("Invalid choice");
		}
		System.out.println("Result of respective operations : "+result);
	}

}
