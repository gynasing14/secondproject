package week2.day2;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class OddPositionSet {

	public static void main(String[] args) {
		Set<Integer> set = new LinkedHashSet<>();
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(4);
		set.add(5);
		System.out.println(set);
		List<Integer> ls = new ArrayList<>();
		ls.addAll(set);
		for (Integer mlist : ls) {
			if(mlist % 2 != 0) {
				System.out.println(mlist);
			}
		}
		
		
	}

}
