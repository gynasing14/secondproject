package dailychallenges;

import java.util.Scanner;

public class SumArray {

	public static void main(String[] args) {
		//Array Declaration with size
		int arr[] = new int[6];
		
		int arraySum = 0;
		
		//Scanner object creation 
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Array values : ");
		
		//for loop for get user inputs for each index
		for(int i=0; i<arr.length;i++) {
			arr[i] = scan.nextInt();
		}
		
		for (int arrayValues : arr) {
			arraySum += arrayValues;
		}
			
		System.out.println("Sum of numbers in an array :" +arraySum);
	}

}
