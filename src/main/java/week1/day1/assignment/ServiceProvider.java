package week1.day1.assignment;

import java.util.Scanner;

public class ServiceProvider {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter 3 digits of mobile number :");
		long mobileNumber = scan.nextLong();
		if(mobileNumber == 944) {
			System.out.println("BSNL");
		}else if(mobileNumber == 900) {
			System.out.println("Airtel");
		}else if(mobileNumber == 897) {
			System.out.println("Idea");
		}else if(mobileNumber == 630) {
			System.out.println("JIO");
		}else {
			System.out.println("Not a service provider");
		}
	}

}
