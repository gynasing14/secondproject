package week2.day1;

public class Television implements Entertainment{
	public void changeChannel() {
		System.out.println("changing channel");
	}
	public void volumeAdjustment() {
		System.out.println("Increasing/Decreasing volume");
	}
	public void contrastAdjustment() {
		System.out.println("Increase/Decrease brightness");
	}
	@Override
	public void powerBackup() {
		// TODO Auto-generated method stub
		System.out.println("power backup method");
	}
	@Override
	public void comedyShow() {
		// TODO Auto-generated method stub
		System.out.println("comedy show");
	}
	@Override
	public void news() {
		// TODO Auto-generated method stub
		System.out.println("news");
	}
}
