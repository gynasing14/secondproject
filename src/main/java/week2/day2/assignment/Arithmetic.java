package week2.day2.assignment;

import java.util.Scanner; 


public class Arithmetic {

	public static void main(String[] args) {
		int result = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter First value : ");
		int num1 = scan.nextInt();
		System.out.println("Enter Second value : ");
		int num2 = scan.nextInt();
		System.out.println("Enter your choice to perform arithmetic operations : ");
		String choice = scan.next();
		switch(choice) {
		case "Add" :
			result = num1 + num2;
			break;
		case "Sub" :
			result = num1 - num2;
			break;
		case "Multiply" :
			result = num1 * num2;
			break;
		case "Division" :
			result = num1 / num2;
			break;
		default:
			System.out.println("Invalid choice");
			break;
		}
System.out.println(result);
	}

}
