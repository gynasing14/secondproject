package week1.day2.assignment;

import java.util.Scanner;

public class OddDigitSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Number :");
		Integer num = scan.nextInt();
		int sum= 0;
		while(num > 0) {
			int digit = num % 10;
			if(digit % 2 !=0) {
				sum += digit;
			}
			num /= 10;
		}
		System.out.println(sum);
	}
}
