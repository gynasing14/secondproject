package week3.day4;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		Thread.sleep(3000);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		driver.findElementById("chkSelectDateOnly").click();
		List<WebElement> allRows = table.findElements(By.tagName("tr"));
		System.out.println(allRows.size());
		WebElement secRow = allRows.get(1);
		
		List<WebElement> allColums = secRow.findElements(By.tagName("td"));
		System.out.println(allColums.size());
		String text = allColums.get(1).getText();
		System.out.println(text);
		
		//Print column Value
		
//		for(int i = 0;i<=allRows.size()-1;i++) {
//			List<WebElement> allColum = allRows.get(i).findElements(By.tagName("td"));
//			System.out.println(allColum.get(1).getText());
//		}
		
		List<WebElement> allColum = allRows.get(2).findElements(By.tagName("td"));
		for(int i = 0;i<=allRows.size()-1;i++) {
			System.out.println(allColum.get(2).getText());
		}
	
//		for (WebElement secondColValues : allRows) {
//			System.out.println(allRows.get(0).getText());
//		}
		
		
		
	}

}
