package dailychallenges;

import java.util.Scanner;

public class PalindromeOrNot {

	public static void main(String[] args) {
		String reverse = ""; 
		Scanner scan = new Scanner(System.in);
		String string = scan.next();
		int length = string.length();
		for(int i = length - 1; i>=0;i--) {
			 reverse = reverse + string.charAt(i);
		}
		if(string.equals(reverse)) {
			System.out.println("Palindrome");
		}else {
			System.out.println("Not Palindrome");
		}
	}

}
