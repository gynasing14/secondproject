package week4.day1.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandle {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();

//		driver.findElementByXPath("//img[@alt='Lookup']").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();

		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
		driver.findElementByName("firstName").sendKeys("Binu");
		driver.findElementByXPath("//button[text()='Find Leads']").click();

		//		WebElement table = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
		//		
		//		List<WebElement> allRows = table.findElements(By.tagName("tr"));
		//		System.out.println(allRows.size());
		//		WebElement firstRow = allRows.get(0);
		//		
		//		List<WebElement> allColums = firstRow.findElements(By.tagName("td"));
		//		allColums.get(0).findElement(By.linkText("10025")).click();
		//		driver.findElementByLinkText("10025").click();
		driver.close();
		
//		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
//		Set<String> allWindow = driver.getWindowHandles();
//		List<String> lst1 = new ArrayList<>();
//		lst.addAll(allWindow);
//		driver.switchTo().window(lst1.get(1));
//		driver.findElementByName("firstName").sendKeys("Revthy");
//		driver.findElementByXPath("//button[text()='Find Leads']").click();
//		driver.close();
		
//		driver.findElementByLinkText("Merge").click();
		
		driver.findElementByXPath("//a[text()='Merge']").click();
		driver.switchTo().alert().accept();
		
		
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("10025");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		
		driver.close();

	}

}
