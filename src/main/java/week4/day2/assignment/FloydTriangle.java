package week4.day2.assignment;

public class FloydTriangle {

	public static void main(String[] args) {
		int i,j; 
		int val = 1;
		int n = 4;
		for( i = 1 ; i <= n; i++) {
			for(j = 1; j <= i; j++) {
				System.out.print(val + " ");
				val++;
			}
			System.out.println();
		}
	}

}
