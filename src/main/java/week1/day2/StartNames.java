package week1.day2;

public class StartNames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String nameList = "babu,abhi,ram,.anniyan,sarath,gopi";
		String[] split =nameList.split(",");
		for(String s: split) {
			/*
			 * Name Starts with a
			 */
			if(s.startsWith("a")) {
				System.out.println("Name starts with a: "+s);
			}
			/*
			 * Name contains a
			 */
			if(s.contains("a")) {
				System.out.println("Name Contains a:"+s);
			}
		}
	}
	
	//String str = "Amazon India Private Limited";
	
}


