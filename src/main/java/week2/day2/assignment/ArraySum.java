package week2.day2.assignment;


//No User input
public class ArraySum {

	public static void main(String[] args) {
		int[] arr = {25,26,9,18,3,12};
		int sum = 0;
		for (int num : arr) {
			sum += num;
		}
		System.out.println("Sum of array Elements : "+sum);
	}

}
