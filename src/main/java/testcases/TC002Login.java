package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002Login extends ProjectMethods{
	@Test
	public void FindLeads() {
		login();
		WebElement eleCreateLeadLink = locateElement("linktext", "Create Lead");
		click(eleCreateLeadLink);
		WebElement eleFindLeadLink = locateElement("linktext", "Find Leads");
		click(eleFindLeadLink);
//		WebElement eleLeadId = locateElement("name", "id");
//		type(eleLeadId, "11812");
		WebElement webeleFirstName = locateElement("xpath", "(//input[@name=\"firstName\"])[3]");
		type(webeleFirstName, "Gynanasekar");
		WebElement webeleLastName = locateElement("xpath", "(//input[@name=\"lastName\"])[3]");
		type(webeleLastName, "Singaravel");
		WebElement webeleCompanyName = locateElement("xpath", "(//input[@name=\"companyName\"])[2]");
		type(webeleCompanyName, "IVTL");
		WebElement eleFindLeads = locateElement("xpath","//button[text()=\"Find Leads\"]");
		click(eleFindLeads);

	}
	

}
