package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTest {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
		driver.findElementByXPath("//button[text()='✕']").click();
		driver.findElementByName("q").sendKeys("iPhone X");
		driver.findElementByXPath("//button[@type='submit']").click();
		String price = driver.findElementByXPath("//div[@class='_1vC4OE']").getText();
		System.out.println(price);
		
	}

}
