package week1.day1.assignment;

public class MobilePhone {
	public int currentBalance = 9;
	
	public void sendSMS(long mobileNumber, String SMS) {
		System.out.println("Sending SMS to " +mobileNumber +"  "+SMS);
	}
	
	
	public void callContact(long mobileNumber) {
		System.out.println("Calling " +mobileNumber);
	}
}
