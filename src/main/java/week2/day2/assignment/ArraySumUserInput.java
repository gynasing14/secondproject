package week2.day2.assignment;

import java.util.Scanner;

//User Input
public class ArraySumUserInput {

	public static void main(String[] args) {
		int arr[] = new int[6];
		int sum = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Array Elements : ");
		for (int i = 0; i < arr.length; i++) {
			arr[i] = scan.nextInt();
		}
		//System.out.println(arr);
		for (int num : arr) {
			sum += num;
		}
		System.out.println("Sum of Array Elements : " +sum);
	}

}
