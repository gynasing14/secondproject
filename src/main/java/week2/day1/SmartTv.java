package week2.day1;

public class SmartTv extends Television{
	public void connectivity(){
		System.out.println("connecting bluetooth/wifi");
	}
	public void watchOnline(){
		System.out.println("watch online videos");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Television tv = new Television();
		tv.changeChannel();
		SmartTv smarttv = new SmartTv();
		smarttv.contrastAdjustment();
		smarttv.watchOnline();
		//call interface methods
		Entertainment entertainment =new SmartTv();
		entertainment.comedyShow();
		Entertainment entertainment1 =new Television();
		entertainment1.powerBackup();
	}

}
