package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReport {
	@Test
	public  void extentReport() throws IOException {
		// TODO Auto-generated method stub
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger = extent.createTest("TC001_CreateLead","Create New Lead");
		
		logger.log(Status.PASS, "The Data DemoSalesManager entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		logger.log(Status.PASS, "The Data crmsfa entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
		logger.log(Status.PASS, "The button login clicked successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
		
		extent.flush();
	}

}
