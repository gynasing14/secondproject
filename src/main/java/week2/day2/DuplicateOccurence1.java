package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class DuplicateOccurence1 {

	public static void main(String[] args) {
		String companyName = "Cognizant";
		char[] arr = companyName.toCharArray();
		Map<Character,Integer> map =new HashMap<>();
		for (char charList : arr) {
			if (map.containsKey(charList)) {
				map.put(charList, map.get(charList)+1);
			}
			else {
				map.put(charList, 1);
			}
//			System.out.println(c);
		}
		System.out.println(map);
	}

}
