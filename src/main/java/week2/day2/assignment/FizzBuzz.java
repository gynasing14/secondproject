package week2.day2.assignment;

import java.util.Scanner;

public class FizzBuzz {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number : ");
		int num = scan.nextInt();
		for (int i = 1; i <= num; i++) {
			if((i%3 == 0) && (i%5 == 0)) {
				System.out.println("Fizz Buzz");
			}else if(i%5 == 0) {
				System.out.println("Buzz");
			}else if(i%3 == 0) {
				System.out.println("Fizz");
			}else {
				System.out.println(i);
			}
			
//			if(i%3 == 0) {
//				if(i%5 == 0) {
//					System.out.println("FizzBuzz");
//				}else {
//					System.out.println("Buzz");
//				}
//			}else if(i%3 == 0) {
//				System.out.println("Fizz");
//			}else {
//				System.out.println(i);
//			}
//			
		}
	}

}
