package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC001Login extends ProjectMethods{

	@Test
	public void CreateLead() {
		login();
		WebElement eleCreateLeadLink = locateElement("linktext", "Create Lead");
		click(eleCreateLeadLink);
		WebElement eleCompanyName = locateElement("createLeadForm_companyName");
		type(eleCompanyName, "IVTL");
		WebElement eleFirstName = locateElement("createLeadForm_firstName");
		type(eleFirstName, "Gynanasekar");
		WebElement eleLastName = locateElement("createLeadForm_lastName");
		type(eleLastName, "Singaravel");
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Cold Call");
		WebElement eleMarketingCampaign = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(eleMarketingCampaign, 1);
		WebElement eleFirstNameLocal = locateElement("id","createLeadForm_firstNameLocal");
		type(eleFirstNameLocal, "Gynana");
		WebElement eleLastNameLocal = locateElement("id","createLeadForm_lastNameLocal");
		type(eleLastNameLocal, "Sekar");
		WebElement eleSalutation = locateElement("id","createLeadForm_personalTitle");
		type(eleSalutation, "Information about me");


		//BirthDate

		WebElement eleTitle = locateElement("id","createLeadForm_generalProfTitle");
		type(eleTitle, "Title Information");
		WebElement eleDepartment = locateElement("id","createLeadForm_departmentName");
		type(eleDepartment, "Associate");
		WebElement eleAnnualRevenue = locateElement("id","createLeadForm_annualRevenue");
		type(eleAnnualRevenue, "1C");
		WebElement elePrefferedCurrency = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingValue(elePrefferedCurrency, "INR");
		WebElement eleIndustry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleIndustry, 1);
		WebElement eleNoOfEmployees = locateElement("id", "createLeadForm_numberEmployees");
		type(eleNoOfEmployees, "1");
		WebElement eleOwnership = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingValue(eleOwnership, "OWN_PROPRIETOR");
		WebElement eleSicCode = locateElement("id", "createLeadForm_sicCode");
		type(eleSicCode, "007");
		WebElement eleTickerSymbol = locateElement("id", "createLeadForm_tickerSymbol");
		type(eleTickerSymbol, "*");
		WebElement eleDescription = locateElement("id", "createLeadForm_description");
		type(eleDescription, "Nothing");
		WebElement eleImportantNote = locateElement("id", "createLeadForm_importantNote");
		type(eleImportantNote, "Nothing");

		WebElement eleCreateLead = locateElement("name","submitButton");
		click(eleCreateLead);


		//Edit Lead

		//		WebElement eleEdit = locateElement("linktext", "Edit");
		//		click(eleEdit);
		//		WebElement eleUpdateDescription = locateElement("id", "updateLeadForm_description");
		//		type(eleUpdateDescription, "Employee of IVTL");
		//		WebElement eleUpdate = locateElement("xpath", "//input[@value=\"Update\"]");
		//		click(eleUpdate);

		//Duplicate Lead
		//		WebElement eleDuplicateLead = locateElement("linktext", "Duplicate Lead");
		//		click(eleDuplicateLead);
		//		WebElement eleAreaCode = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		//		type(eleAreaCode, "123");
		//		WebElement eleDuplicate = locateElement("xpath", "//input[@value=\"Create Lead\"]");
		//		click(eleDuplicate);

		//Delete Lead
		//		WebElement eleDelete = locateElement("linktext", "Delete");
		//		click(eleDelete);

		// Merge Leads

		//		WebElement eleMergeLeadLink = locateElement("linktext", "Merge Leads");
		//		click(eleMergeLeadLink);
		//		WebElement webimg1 = locateElement("xpath", "(//img[@alt=\"Lookup\"])[1]");
		//		click(webimg1);
		//		switchToWindow(1);
		//		WebElement eleLeadId1 = locateElement("name", "id");
		//		type(eleLeadId1, "11812");
		//		WebElement eleFindLeads1 = locateElement("xpath","//button[text()=\"Find Leads\"]");
		//		click(eleFindLeads1);
		//		WebElement eleFirstTableValue = locateElement("xpath","//tbody/tr/td/div/a[text()=\"11812\"]");
		//		click(eleFirstTableValue);

		//				switchToWindow(0);
		//				WebElement webimg2 = locateElement("xpath", "(//img[@alt=\"Lookup\"])[2]");
		//				click(webimg2);
		//				switchToWindow(1);
		//				WebElement eleLeadId2 = locateElement("name", "id");
		//				type(eleLeadId2, "11813");
		//				WebElement eleFindLeads2 = locateElement("xpath","//button[text()=\"Find Leads\"]");
		//				click(eleFindLeads2);
		//				WebElement eleSecondTableValue = locateElement("xpath","//tbody/tr/td/div/a[text()=\"11813\"]");
		//				click(eleSecondTableValue);





		//Find Leads

				























	}

}












