package week3.day3;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("Gyna@123");
		driver.findElementById("userRegistrationForm:password").sendKeys("password@123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("password@123");
		WebElement securityQuestion = driver.findElementById("userRegistrationForm:securityQ");
		Select firstDropdown = new Select(securityQuestion);
		
		firstDropdown.selectByValue("0");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("AADHI");
		WebElement preferredLanguage = driver.findElementById("userRegistrationForm:prelan");
		Select secondDropdown = new Select(preferredLanguage);
		secondDropdown.selectByValue("en");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Gynana");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("sekar");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("S");
		driver.findElementByName("userRegistrationForm:gender").click();
		driver.findElementByName("userRegistrationForm:maritalStatus").click();
		WebElement dobDate = driver.findElementById("userRegistrationForm:dobDay");
		Select thirdDropdown = new Select(dobDate);
		thirdDropdown.selectByValue("14");
		WebElement dobMonth = driver.findElementById("userRegistrationForm:dobMonth");
		Select forthDropdown = new Select(dobMonth);
		forthDropdown.selectByValue("11");
		WebElement dobYear = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select fifthDropdown = new Select(dobYear);
		fifthDropdown.selectByValue("1994");
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select sixthDropdown = new Select(occupation);
		sixthDropdown.selectByValue("1");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("123456789987");
		driver.findElementById("userRegistrationForm:idno").sendKeys("BVMPG5778P");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select seventhDropdown = new Select(country);
		seventhDropdown.selectByValue("94");
		driver.findElementById("userRegistrationForm:email").sendKeys("gynansekar@gamil.com");
		
		driver.findElementById("userRegistrationForm:mobile").sendKeys("1234567890");
		WebElement Nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select Nty = new Select (Nationality);
		Nty.selectByValue("94");
		driver.findElementById("userRegistrationForm:address").sendKeys("3/105,MGM");
		driver.findElementById("userRegistrationForm:street").sendKeys("Krishna Street");
		driver.findElementById("userRegistrationForm:area").sendKeys("okkiyampet");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600091",Keys.TAB);
		Thread.sleep(5000);

		WebElement City = driver.findElementById("userRegistrationForm:cityName");
		Select Cty = new Select (City);
		Cty.selectByValue("Kanchipuram");
		Thread.sleep(5000);
		WebElement PostOffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select Pst = new Select (PostOffice);
		Pst.selectByValue("Madipakkam S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("04412345678");
		driver.findElementById("userRegistrationForm:resAndOff:1").click();
		driver.findElementById("userRegistrationForm:addresso").sendKeys("3/105");
		driver.findElementById("userRegistrationForm:streeto").sendKeys("Krishna Street");
		driver.findElementById("userRegistrationForm:areao").sendKeys("karapakkam");
		WebElement CtryOff = driver.findElementById("userRegistrationForm:countrieso");
		Select Coff = new Select (CtryOff);
		Coff.selectByValue("94");
		driver.findElementById("userRegistrationForm:pincodeo").sendKeys("600091", Keys.TAB);
		Thread.sleep(5000);
		WebElement CityOff = driver.findElementById("userRegistrationForm:cityNameo");
		Select CtyOff = new Select (CityOff);
		CtyOff.selectByValue("Kanchipuram");
		Thread.sleep(5000);
		WebElement PostOfficeOff = driver.findElementById("userRegistrationForm:postofficeNameo");
		Select PstOff = new Select (PostOfficeOff);
		PstOff.selectByValue("Madipakkam S.O");
		driver.findElementById("userRegistrationForm:landlineo").sendKeys("04412345987");
	}

}
