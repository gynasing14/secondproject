package week1.day2.assignment;

import java.util.Scanner;

public class FindPrimeNumber {

	public static void main(String[] args) {
		boolean flag = false;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value : ");
		int num = sc.nextInt();
		for(int i=2;i<=num/2;++i) {
			if(num%i == 0) {
				flag = true;
				break;
			}
		}
		if(!flag) {
			System.out.println("Prime");
		}else {
			System.out.println("Not Prime");
		}
	}
}
