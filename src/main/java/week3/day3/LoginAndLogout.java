package week3.day3;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginAndLogout {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
//		driver.findElementById("createLeadForm_companyName").sendKeys("Pradeep & Co");
//		driver.findElementById("createLeadForm_firstName").sendKeys("G");
//		driver.findElementById("createLeadForm_lastName").sendKeys("S");
//		driver.findElementByClassName("smallSubmit").click();
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source); 
		dd.selectByVisibleText("Public Relation");
		WebElement source1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(source1); 
		dd1.selectByValue("CATRQ_AUTOMOBILE");
		
		
	}
}
