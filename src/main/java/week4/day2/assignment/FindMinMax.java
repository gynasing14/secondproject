package week4.day2.assignment;

public class FindMinMax {

	public static void main(String[] args) {
		int arr[] = {10,15,6,7,18,3,28,5};
		
		int min = arr[0];
		int max = arr[0];
		
		for(int i = 1; i < arr.length; i++ ) {
			if(arr[i] < min) {
				min = arr[i];
			}
			if(arr[i] > max) {
				max = arr[i];
			}
		}
		System.out.println(min);
		System.out.println(max);
	}

}
